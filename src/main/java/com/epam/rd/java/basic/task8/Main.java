package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Container;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.XMLConstants;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			System.out.println(args.length);
			return;
		}
		String xsdFile = "input.xsd";
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		System.out.println("~~~~~~~~ DOM ~~~~~~~~~~");
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Container containerDOM = domController.parseFromXML();
		System.out.println(containerDOM.toString());

		// sort (case 1)
		containerDOM.sortByNameFlower();
		// PLACE YOUR CODE HERE
		
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.parseToXML(containerDOM, outputXmlFile);
		System.out.println("isValid --> " + isValidOutputXML(outputXmlFile, xsdFile));
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		System.out.println("~~~~~~~~ SAX ~~~~~~~~~");
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Container containerSAX = saxController.parseFromXML();
		System.out.println("SAX isValid? --> " + isValidOutputXML(outputXmlFile, xsdFile));

		
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		containerSAX.sortByOriginFlower();

		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		saxController.parseToXML(containerSAX, outputXmlFile);
		//System.out.println("SAX isValid? --> " + isValidOutputXML(outputXmlFile, xsdFile));
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		System.out.println("~~~~~~~ StAX ~~~~~~~~~");

		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Container containerStAX = staxController.parseFromXML();
		System.out.println(containerStAX.toString());
		
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		containerStAX.sortByAveLenFlower();
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.parseToXML(containerStAX,outputXmlFile);
		// PLACE YOUR CODE HERE
	}
	private static boolean isValidOutputXML(String pathToXML, String pathToXSD) throws SAXNotSupportedException, SAXNotRecognizedException {
		if(pathToXML == null){
			return false;
		}
		SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		sf.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES,false);
		try{
			Schema s = sf.newSchema(new File(pathToXSD));
			Validator v = s.newValidator();
			v.validate(new StreamSource(new File(pathToXML)));
		} catch (SAXException | IOException e) {
			throw new RuntimeException(e);
			//return false;
		}
		return true;
	}
}
