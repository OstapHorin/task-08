package com.epam.rd.java.basic.task8.entity.describers;

public class AveLenFlower {
    private int value;
    private String measure;

    public AveLenFlower(int value, String measure) {
        this.value = value;
        this.measure = measure;
    }

    public AveLenFlower() {
    }

    @Override
    public String toString() {
        return "AveLenFlower{" +
                "value=" + value +
                ", measure='" + measure + '\'' +
                '}';
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }
}
