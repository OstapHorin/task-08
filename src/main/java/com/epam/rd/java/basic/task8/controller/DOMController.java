package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Container;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.describers.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLInputFactory;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Controller for DOM parser.
 */
public class DOMController {


	private String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	public void parseToXML (Container container, String toFile) {
		try(FileOutputStream fos = new FileOutputStream(toFile)){
			Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

			Element root = parseRootElement(document, container);
			document.appendChild(root);

			TransformerFactory factory = TransformerFactory.newInstance();
			factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			Transformer transformer = factory.newTransformer();

			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			DOMSource source = new DOMSource(document);
			StreamResult result = new StreamResult(fos);

			transformer.transform(source, result);

		} catch (IOException | ParserConfigurationException | TransformerException e) {
			throw new RuntimeException(e);
		}
	}
	public Container parseFromXML() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = dbf.newDocumentBuilder();
		Document document = builder.parse(new File(xmlFileName));
		document.getDocumentElement().normalize();

		return parsingLogicOfDOMFromXML(document.getDocumentElement());
	}

	private static Element parseRootElement(Document document, Container container){
		Element elem = document.createElement("flowers");
		for(Map.Entry<String,String> attr : container.getAttrs().entrySet()){
			elem.setAttribute(attr.getKey(), attr.getValue());
		}
		for(Flower flower : container.getFlowers()){
			elem.appendChild(parseFlowerElement(document, flower));
		}
		return elem;
	}

	private static Element parseFlowerElement(Document document, Flower flower){
		Element elem = document.createElement("flower");

		Element name = document.createElement("name");
		Element soil = document.createElement("soil");
		Element origin = document.createElement("origin");
		Element multiplying = document.createElement("multiplying");
		Element vp = parseVisualParametersElement(document, flower.getVisualParameters());
		Element gt = parseGrowingTipsElement(document, flower.getGrowingTips());

		name.setTextContent(flower.getName());
		soil.setTextContent(flower.getSoil());
		origin.setTextContent(flower.getOrigin());
		multiplying.setTextContent(flower.getMultiplying());

		elem.appendChild(name);
		elem.appendChild(soil);
		elem.appendChild(origin);
		elem.appendChild(vp);
		elem.appendChild(gt);
		elem.appendChild(multiplying);

		return elem;
	}
	private static Element parseGrowingTipsElement(Document document, GrowingTips growingTips){
		Element elem = document.createElement("growingTips");
		Element t = parseTemperatureElement(document, growingTips.getTemperature());
		Element l = document.createElement("lighting");
		Element w = parseWateringElement(document, growingTips.getWatering());

		l.setAttribute("lightRequiring", growingTips.getLighting());

		elem.appendChild(t);
		elem.appendChild(l);
		elem.appendChild(w);

		return elem;
	}

	private static Element parseWateringElement(Document document, Watering watering){
		Element elem = document.createElement("watering");
		elem.setAttribute("measure", watering.getMeasure());
		elem.setTextContent(String.valueOf(watering.getWater()));
		return elem;
	}

	private static Element parseTemperatureElement(Document document, Temperature temperature){
		Element elem = document.createElement("tempreture");
		elem.setAttribute("measure", temperature.getMeasure());
		elem.setTextContent(String.valueOf(temperature.getTemperature()));
		return elem;
	}
	private static Element parseVisualParametersElement(Document document, VisualParameters vp){
		Element elem = document.createElement("visualParameters");
		Element stemC = document.createElement("stemColour");
		Element leafC = document.createElement("leafColour");
		stemC.setTextContent(vp.getStemColour());
		leafC.setTextContent(vp.getLeafColour());
		elem.appendChild(stemC);
		elem.appendChild(leafC);
		elem.appendChild(parseAveLenFlower(document,vp.getAveLenFlower()));

		return elem;
	}

	private static Element parseAveLenFlower(Document document, AveLenFlower alf){
		Element elem = document.createElement("aveLenFlower");
		elem.setAttribute("measure", alf.getMeasure());
		elem.setTextContent(String.valueOf(alf.getValue()));
		return elem;
	}


	private static Container parsingLogicOfDOMFromXML (Element root){
		Container container = new Container(new HashMap<>(), new LinkedList<>());
		NodeList nodes = root.getElementsByTagName("flower");
		for(int i=0; i<root.getAttributes().getLength(); i++){
			Node node = root.getAttributes().item(i);
			container.addAttr(node.getNodeName(), node.getTextContent());
		}
		for(int i=0;i<nodes.getLength();i++){
			Flower flower;
			Element elem = (Element) nodes.item(i);

			flower = parseBasicFlowerParamsWithDomParser(elem);

			VisualParameters vp = parseVisualParametersWithDomFromXML(elem);
			vp.setAveLenFlower(parseAveLenFlowerWithDomFromXML(elem));

			GrowingTips gt = parseBasicGrowingTipsWithDomFromXml(elem);
			gt.setWatering(parseWateringWithDomParserFromXml(elem));
			gt.setTemperature(parseTemperatureWithDomFromXml(elem));

			flower.setGrowingTips(gt);
			flower.setVisualParameters(vp);
			container.addFlower(flower);
		}
		return container;
	}

	private static Temperature parseTemperatureWithDomFromXml(Element element){
		Temperature t = new Temperature();
		Node node = element.getElementsByTagName("tempreture").item(0);
		String measure = node.getAttributes().getNamedItem("measure").getTextContent();
		int value = Integer.parseInt(node.getTextContent());
		t.setMeasure(measure);
		t.setTemperature(value);
		return t;
	}
	private static Watering parseWateringWithDomParserFromXml(Element element){
		Watering wt = new Watering();
		Node node = element.getElementsByTagName("watering").item(0);

		String measure = node.getAttributes().getNamedItem("measure").getTextContent();
		int value = Integer.parseInt(node.getTextContent());
		wt.setMeasure(measure);
		wt.setWater(value);

		return wt;
	}

	private static GrowingTips parseBasicGrowingTipsWithDomFromXml(Element element){
		GrowingTips gt = new GrowingTips();
		Node node = element.getElementsByTagName("growingTips").item(0);
		Element elem = (Element) node;
		String light = elem.getElementsByTagName("lighting")
				.item(0).getAttributes().getNamedItem("lightRequiring").getTextContent();
		gt.setLighting(light);
		return gt;
	}
	private static Flower parseBasicFlowerParamsWithDomParser(Element elem){
		Flower flower = new Flower();

		String name = elem.getElementsByTagName("name").item(0).getTextContent();
		String soil = elem.getElementsByTagName("soil").item(0).getTextContent();
		String origin = elem.getElementsByTagName("origin").item(0).getTextContent();
		String multiplying = elem.getElementsByTagName("multiplying").item(0).getTextContent();

		flower.setName(name);
		flower.setOrigin(origin);
		flower.setSoil(soil);
		flower.setMultiplying(multiplying);

		return flower;
	}
	private static VisualParameters parseVisualParametersWithDomFromXML(Element element){
		VisualParameters vp = new VisualParameters();
		Node node = element.getElementsByTagName("visualParameters").item(0);
		Element elem = (Element) node;

		String stemColour = elem.getElementsByTagName("stemColour").item(0).getTextContent();
		String leafColour = elem.getElementsByTagName("leafColour").item(0).getTextContent();

		vp.setLeafColour(leafColour);
		vp.setStemColour(stemColour);

		return vp;
	}
	private static AveLenFlower parseAveLenFlowerWithDomFromXML(Element element){
		AveLenFlower alf = new AveLenFlower();
		Node node = element.getElementsByTagName("aveLenFlower").item(0);
		Element elem = (Element) node;

		String measure = elem.getAttributes().getNamedItem("measure").getTextContent();
		int value = Integer.parseInt(node.getTextContent());

		alf.setMeasure(measure);
		alf.setValue(value);

		return alf;
	}
}
