package com.epam.rd.java.basic.task8.entity;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class Container {
    private final Map<String,String> attrs;
    private final List<Flower> flowers;

    public Container(Map<String, String> attrs, List<Flower> flowers) {
        this.attrs = attrs;
        this.flowers = flowers;
    }

    public Map<String, String> getAttrs() {
        return attrs;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void addFlower(Flower flower){
        flowers.add(flower);
    }
    public void addAttr(String key, String val){
        attrs.put(key,val);
    }
    public void sortByNameFlower(){
        flowers.sort(Comparator.comparing(Flower::getName));
    }
    public void sortByOriginFlower(){
        flowers.sort(Comparator.comparing(Flower::getOrigin));
    }
    public void sortBySoilFlower(){
        flowers.sort(Comparator.comparing(Flower::getSoil));
    }
    public void sortByAveLenFlower(){
        flowers.sort(Comparator.comparing(flower -> flower.getVisualParameters().getAveLenFlower().getValue()));
    }

    @Override
    public String toString() {
        return "Container{" +
                "attrs=" + attrs +
                ", flowers=" + flowers +
                '}';
    }
}
