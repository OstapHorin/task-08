package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Container;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.describers.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;

	private Container container;
	private Flower flower;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public Container parseFromXML() throws FileNotFoundException, XMLStreamException {
		container = new Container(new HashMap<>(), new ArrayList<>());
		XMLEventReader r = XMLInputFactory.newFactory().createXMLEventReader(new FileInputStream(xmlFileName));
		while(r.hasNext()) {
			XMLEvent e = r.nextEvent();

			if(e.isStartElement()){
				StartElement elem = e.asStartElement();
				String qName = elem.getName().getLocalPart();

				if (qName.equalsIgnoreCase("flowers")) {
					elem.getNamespaces().forEachRemaining(
							namespace -> {
								String prefix = Stream.of(namespace.getName().getPrefix(), namespace.getPrefix())
										.filter(s -> !s.equals(""))
										.collect(Collectors.joining(":"));
								container.addAttr(prefix, namespace.getValue());
							});
					elem.getAttributes().forEachRemaining(
							attribute -> {
								String prefix = Stream.of(attribute.getName().getPrefix(), attribute.getName().getLocalPart())
										.filter(s -> !s.equals(""))
										.collect(Collectors.joining(":"));
								container.addAttr(prefix, attribute.getValue());
							});
				} else if (qName.equalsIgnoreCase("flower")) {
					flower = new Flower();
					container.addFlower(flower);
				} else if (qName.equalsIgnoreCase("name")) {
					e = r.nextEvent();
					flower.setName(e.asCharacters().getData());
				} else if (qName.equalsIgnoreCase("soil")) {
					e = r.nextEvent();
					flower.setSoil(e.asCharacters().getData());
				} else if (qName.equalsIgnoreCase("origin")) {
					e = r.nextEvent();
					flower.setOrigin(e.asCharacters().getData());

				} else if (qName.equalsIgnoreCase("visualParameters")) {
					flower.setVisualParameters(new VisualParameters());
				} else if (qName.equalsIgnoreCase("stemColour")) {
					e = r.nextEvent();
					flower.getVisualParameters().setStemColour(e.asCharacters().getData());
				} else if (qName.equalsIgnoreCase("leafColour")) {
					e = r.nextEvent();
					flower.getVisualParameters().setLeafColour(e.asCharacters().getData());
				} else if (qName.equalsIgnoreCase("aveLenFlower")) {
					AveLenFlower aveLenFlower = new AveLenFlower();
					aveLenFlower.setMeasure(elem.getAttributeByName(new QName("measure")).getValue());
					e = r.nextEvent();
					aveLenFlower.setValue(Integer.parseInt(e.asCharacters().getData()));
					flower.getVisualParameters().setAveLenFlower(aveLenFlower);

				} else if (qName.equalsIgnoreCase("growingTips")) {
					flower.setGrowingTips(new GrowingTips());
				} else if (qName.equalsIgnoreCase("tempreture")) {
					Temperature temperature = new Temperature();
					temperature.setMeasure(elem.getAttributeByName(new QName("measure")).getValue());
					e = r.nextEvent();
					temperature.setTemperature(Integer.parseInt(e.asCharacters().getData()));
					flower.getGrowingTips().setTemperature(temperature);
				} else if (qName.equalsIgnoreCase("lighting")) {
					flower.getGrowingTips().setLighting(elem.getAttributeByName(
							new QName("lightRequiring")).getValue());
				} else if (qName.equalsIgnoreCase("watering")) {
					Watering watering = new Watering();
					watering.setMeasure(elem.getAttributeByName(new QName("measure")).getValue());
					e = r.nextEvent();
					watering.setWater(Integer.parseInt(e.asCharacters().getData()));
					flower.getGrowingTips().setWatering(watering);

				} else if (qName.equalsIgnoreCase("multiplying")) {
					e = r.nextEvent();
					flower.setMultiplying(e.asCharacters().getData());
				}
			}
		}
		return container;
	}

	public void parseToXML(Container container, String file) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory f = XMLOutputFactory.newInstance();
		XMLEventFactory ef = XMLEventFactory.newInstance();

		XMLEventWriter w = f.createXMLEventWriter(new FileOutputStream(file));

		parseContainer(container, w, ef);

		w.add(ef.createEndDocument());
		w.flush();
		w.close();
	}

	private void parseContainer(Container container, XMLEventWriter writer, XMLEventFactory eventFactory) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("", "", "flowers"));

		for(Map.Entry<String, String> entry : container.getAttrs().entrySet()){
			writer.add(eventFactory.createAttribute(entry.getKey(),entry.getValue()));
		}

		for(Flower f : container.getFlowers()){
			parseFlower(f, writer, eventFactory);
		}

		writer.add(eventFactory.createEndElement("","","flowers"));

	}

	private void parseFlower(Flower f, XMLEventWriter writer, XMLEventFactory eventFactory) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("","", "flower"));
			writer.add(eventFactory.createStartElement("","", "name"));
				writer.add(eventFactory.createCharacters(f.getName()));
			writer.add(eventFactory.createEndElement("","", "name"));

			writer.add(eventFactory.createStartElement("","", "soil"));
				writer.add(eventFactory.createCharacters(f.getSoil()));
			writer.add(eventFactory.createEndElement("","", "soil"));

			writer.add(eventFactory.createStartElement("","", "origin"));
				writer.add(eventFactory.createCharacters(f.getOrigin()));
			writer.add(eventFactory.createEndElement("","", "origin"));

			parseVisualParameters(f.getVisualParameters(), writer, eventFactory);
			parseGrowingTips(f.getGrowingTips(),writer,eventFactory);

			writer.add(eventFactory.createStartElement("","","multiplying"));
				writer.add(eventFactory.createCharacters(f.getMultiplying()));
			writer.add(eventFactory.createEndElement("","","multiplying"));

		writer.add(eventFactory.createEndElement("","","flower"));
	}

	private void parseGrowingTips(GrowingTips gt, XMLEventWriter writer,XMLEventFactory eventFactory) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("","","growingTips"));

	    writer.add(eventFactory.createStartElement("","","tempreture"));
			writer.add(eventFactory.createAttribute("measure", gt.getTemperature().getMeasure()));
			writer.add(eventFactory.createCharacters(String.valueOf(gt.getTemperature().getTemperature())));
		writer.add(eventFactory.createEndElement("","","tempreture"));


		writer.add(eventFactory.createStartElement("","","lighting"));
			writer.add(eventFactory.createAttribute("lightRequiring", gt.getLighting()));
		writer.add(eventFactory.createEndElement("","","lighting"));
		writer.add(eventFactory.createStartElement("","","watering"));
			writer.add(eventFactory.createAttribute("measure", gt.getWatering().getMeasure()));
			writer.add(eventFactory.createCharacters(String.valueOf(gt.getWatering().getWater())));
		writer.add(eventFactory.createEndElement("","","watering"));



		writer.add(eventFactory.createEndElement("","","growingTips"));
	}
	private void parseVisualParameters(VisualParameters vp, XMLEventWriter writer, XMLEventFactory eventFactory) throws XMLStreamException {
		writer.add(eventFactory.createStartElement("","","visualParameters"));
				writer.add(eventFactory.createStartElement("","","stemColour"));
					writer.add(eventFactory.createCharacters(vp.getStemColour()));
				writer.add(eventFactory.createEndElement("","","stemColour"));

				writer.add(eventFactory.createStartElement("","","leafColour"));
					writer.add(eventFactory.createCharacters(vp.getLeafColour()));
				writer.add(eventFactory.createEndElement("","","leafColour"));

				writer.add(eventFactory.createStartElement("","","aveLenFlower"));
					writer.add(eventFactory.createAttribute("measure", vp.getAveLenFlower().getMeasure()));
					writer.add(eventFactory.createCharacters(String.valueOf(vp.getAveLenFlower().getValue())));
				writer.add(eventFactory.createEndElement("","","aveLenFlower"));
		writer.add(eventFactory.createEndElement("","","visualParameters"));
	}

}