package com.epam.rd.java.basic.task8.entity.describers;

public class Watering {
    private int water;
    private String measure;

    public Watering(int water, String measure) {
        this.water = water;
        this.measure = measure;
    }

    public Watering() {
    }

    @Override
    public String toString() {
        return "Watering{" +
                "water=" + water +
                ", measure='" + measure + '\'' +
                '}';
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }
}
