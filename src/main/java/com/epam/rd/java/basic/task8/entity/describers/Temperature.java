package com.epam.rd.java.basic.task8.entity.describers;

public class Temperature {
    private int temperature;
    private String measure;

    public Temperature(int temperature, String measure) {
        this.temperature = temperature;
        this.measure = measure;
    }

    public Temperature() {
    }

    @Override
    public String toString() {
        return "Temperature{" +
                "temperature=" + temperature +
                ", measure='" + measure + '\'' +
                '}';
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }
}
