package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Container;
import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.describers.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private Container container;
	private Flower flower;

	private StringBuilder sb = new StringBuilder();

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public Container parseFromXML() throws ParserConfigurationException, SAXException, IOException {
		//System.out.println("parseFromXml#SAXController");
		container = new Container(new HashMap<>(), new ArrayList<>());
		//System.out.println("container --> " + container.toString());
		SAXParserFactory pf = SAXParserFactory.newInstance();
		SAXParser p = pf.newSAXParser();
		p.parse(xmlFileName, this);
		return container;
	}

	public void parseToXML(Container container, String file) throws FileNotFoundException, XMLStreamException {
		XMLOutputFactory f = XMLOutputFactory.newInstance();
		XMLStreamWriter w = f.createXMLStreamWriter(new FileOutputStream(file));

		parseContainer(container, w);

		//w.writeEndElement();
		w.flush();
		w.close();
	}
	private void parseContainer(Container container, XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartElement("flowers");
			for(Map.Entry<String, String> entry : container.getAttrs().entrySet()){
				writer.writeAttribute(entry.getKey(),entry.getValue());
			}
			for(Flower fl : container.getFlowers()){
				parseFlower(fl, writer);
			}
		writer.writeEndElement();
	}
	private void parseFlower(Flower flower, XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartElement("flower");
			writer.writeStartElement("name");
				writer.writeCharacters(flower.getName());
			writer.writeEndElement();

			writer.writeStartElement("soil");
				writer.writeCharacters(flower.getSoil());
			writer.writeEndElement();

			writer.writeStartElement("origin");
				writer.writeCharacters(flower.getOrigin());
			writer.writeEndElement();

			parseVisualParameters(flower.getVisualParameters(), writer);

			parseGrowingTips(flower.getGrowingTips(), writer);

			writer.writeStartElement("multiplying");
				writer.writeCharacters(flower.getMultiplying());
			writer.writeEndElement();
		writer.writeEndElement();
	}

	private void parseGrowingTips(GrowingTips gt, XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartElement("growingTips");
			writer.writeStartElement("tempreture");
				writer.writeAttribute("measure", gt.getTemperature().getMeasure());
				writer.writeCharacters(String.valueOf(gt.getTemperature().getTemperature()));
			writer.writeEndElement();

			writer.writeStartElement("lighting");
				writer.writeAttribute("lightRequiring", gt.getLighting());
			writer.writeEndElement();

			writer.writeStartElement("watering");
				writer.writeAttribute("measure", gt.getWatering().getMeasure());
				writer.writeCharacters(String.valueOf(gt.getWatering().getWater()));
			writer.writeEndElement();
		writer.writeEndElement();
	}
	private void parseVisualParameters(VisualParameters vp, XMLStreamWriter writer) throws XMLStreamException {
		writer.writeStartElement("visualParameters");
			writer.writeStartElement("stemColour");
				writer.writeCharacters(vp.getStemColour());
			writer.writeEndElement();

			writer.writeStartElement("leafColour");
				writer.writeCharacters(vp.getLeafColour());
			writer.writeEndElement();

			writer.writeStartElement("aveLenFlower");
				writer.writeAttribute("measure",vp.getAveLenFlower().getMeasure());
				writer.writeCharacters(String.valueOf(vp.getAveLenFlower().getValue()));
			writer.writeEndElement();
		writer.writeEndElement();
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		sb.setLength(0);
		if (qName.equalsIgnoreCase("flowers")) {
			for (int i = 0; i < attributes.getLength(); i++) {

				container.addAttr(attributes.getQName(i), attributes.getValue(i));
			}
		} else if (qName.equalsIgnoreCase("flower")) {
			flower = new Flower();

		} else if (qName.equalsIgnoreCase("visualParameters")) {
			flower.setVisualParameters(new VisualParameters());

		} else if (qName.equalsIgnoreCase("aveLenFlower")) {
			AveLenFlower aveLenFlower = new AveLenFlower();
			aveLenFlower.setMeasure(attributes.getValue("measure"));
			flower.getVisualParameters().setAveLenFlower(aveLenFlower);

		} else if (qName.equalsIgnoreCase("growingTips")) {
			flower.setGrowingTips(new GrowingTips());

		} else if (qName.equalsIgnoreCase("tempreture")) {
			Temperature temperature = new Temperature();
			temperature.setMeasure(attributes.getValue("measure"));
			flower.getGrowingTips().setTemperature(temperature);

		} else if (qName.equalsIgnoreCase("lighting")) {
			flower.getGrowingTips().setLighting(attributes.getValue("lightRequiring"));

		} else if (qName.equalsIgnoreCase("watering")) {
			Watering watering = new Watering();
			watering.setMeasure(attributes.getValue("measure"));
			flower.getGrowingTips().setWatering(watering);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase("flower")) {
			container.addFlower(flower);
		} else if (qName.equalsIgnoreCase("name")) {
			flower.setName(sb.toString());
		} else if (qName.equalsIgnoreCase("soil")) {
			flower.setSoil(sb.toString());
		} else if (qName.equalsIgnoreCase("origin")) {
			flower.setOrigin(sb.toString());
		} else if (qName.equalsIgnoreCase("stemColour")) {
			flower.getVisualParameters().setStemColour(sb.toString());
		} else if (qName.equalsIgnoreCase("leafColour")) {
			flower.getVisualParameters().setLeafColour(sb.toString());
		} else if (qName.equalsIgnoreCase("aveLenFlower")) {
			flower.getVisualParameters().getAveLenFlower().setValue(Integer.parseInt(sb.toString()));
		} else if (qName.equalsIgnoreCase("tempreture")) {
			flower.getGrowingTips().getTemperature().setTemperature(Integer.parseInt(sb.toString()));
		} else if (qName.equalsIgnoreCase("watering")) {
			flower.getGrowingTips().getWatering().setWater(Integer.parseInt(sb.toString()));
		} else if (qName.equalsIgnoreCase("multiplying")){
			flower.setMultiplying(sb.toString());
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		sb.append(ch, start,length);
	}
}